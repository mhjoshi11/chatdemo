package com.maya;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;

public class ClientThread implements Runnable {

	public static ArrayList<ClientThread> clients = new ArrayList<>();

	public Socket socket;
	private BufferedReader bufferedReader;
	private BufferedWriter bufferedWriter;
	String clientName;

	public ClientThread(Socket socket) throws IOException {

		this.socket = socket;
		this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		this.clientName = bufferedReader.readLine();

		clients.add(this);
		sendToAll("Hey!! " + clientName + " has entered the chat!");
	}

	@Override
	public void run() {
		String msg;

		while (socket.isConnected()) {
			try {
				msg = bufferedReader.readLine();
				if(msg.equalsIgnoreCase("bye"))
				{
					removeClient(msg);
				}
				else {
				sendToAll(msg);
				}
			} catch (Exception e) {
					try {
						socket.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
			}
		}
	}

	public void sendToAll(String msg) throws IOException {
		
		for (ClientThread clients : clients) {
			if(!clients.clientName.equals(clientName)) {
			
			clients.bufferedWriter.write(msg);
			clients.bufferedWriter.newLine();
			clients.bufferedWriter.flush();
			
			}
		}
	}

	public void removeClient(String msg) throws IOException {
		sendToAll(msg);
		clients.remove(this);
		sendToAll("Hey!! " + clientName + " has left the chat!");
	}

}
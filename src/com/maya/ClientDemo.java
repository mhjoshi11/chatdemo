package com.maya;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClientDemo {

	public static Socket socket;
	private BufferedReader bufferedReader;
	private BufferedWriter bufferedWriter;
	public String username;

	public ClientDemo(String username) throws IOException {
		try {
			socket = new Socket("localhost", 5555);
			this.username = username;
			this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		} catch (IOException e) {
			bufferedReader.close();
			bufferedWriter.close();
		}
	}

	public void writeMessage() throws IOException {
		bufferedWriter.write(username);
		bufferedWriter.newLine();
		bufferedWriter.flush();

		try (Scanner sc = new Scanner(System.in)) {
			while (socket.isConnected()) {
				String msg = sc.nextLine();
				bufferedWriter.write(username + ": " + msg);
				bufferedWriter.newLine();
				bufferedWriter.flush();
			}
//		return msg;
		}
	}

	public void readMessage() {
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				String chatmsg;

				while (socket.isConnected()) {

					try {
						chatmsg = bufferedReader.readLine();
						System.out.println(chatmsg);
					} catch (IOException e) {
						try {
							socket.close();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
		t1.start();
	}

	public static void main(String[] args) throws IOException {

		try (Scanner sc = new Scanner(System.in)) {
			System.out.print("Enter Name for Chat: ");
			String name = sc.nextLine();
			
			ClientDemo client = new ClientDemo(name);
			
			client.readMessage();
			client.writeMessage();
		}
	}

}

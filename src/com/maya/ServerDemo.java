package com.maya;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerDemo {

	private final ServerSocket serverSocket;

	public ServerDemo() throws IOException {

		this.serverSocket = new ServerSocket(5555);
		System.out.println("Server is waiting....");
	}

	public void startServer() throws IOException {
		try {
			while (!serverSocket.isClosed()) {
				Socket socket = serverSocket.accept();
				ClientThread clientThread = new ClientThread(socket);
				System.out.println(clientThread.clientName+" is connected!");
				Thread thread = new Thread(clientThread);
				thread.start();
			}
		} catch (IOException e) {
			serverSocket.close();
		}
	}

	
	public static void main(String[] args) throws IOException {
		try {

			ServerDemo server = new ServerDemo();
			server.startServer();
		} catch (Exception e) {

		}
	}

}

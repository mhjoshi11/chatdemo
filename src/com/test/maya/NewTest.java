package com.test.maya;

import static org.testng.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.testng.annotations.Test;

import com.maya.ClientDemo;
import com.maya.ServerDemo;

public class NewTest {
	
	@Test
	public void testServer() throws IOException {
		
		ServerDemo server = new ServerDemo();
	}

	@Test(dependsOnMethods = { "testServer" })
	public void testClient() throws IOException {
		ClientDemo client = new ClientDemo("maya");
		assertEquals(client.socket.isConnected(), true);
	}
	
//	@Test
//	public void testWriteMsg() throws IOException {
//		
//	  ClientDemo client = new ClientDemo("maya"); 
////	  client.username="maya";
//	  String msg ="test";
//	  InputStream in = new ByteArrayInputStream(msg.getBytes());
//	  System.setIn(in);
//	 
////	  assertEquals("test",  client.writeMessage(msg));
//	}
	  
	  	 
}
